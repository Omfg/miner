package miner;

/**
 * Created by omfg on 13.11.16.
 */
public class BaseAction implements UserAction{
    private final GeneratorBoard generaotr;
    private final Board board;
    private final SaperLogic logic;

    public BaseAction(final SaperLogic logic, final Board board, final GeneratorBoard generaotr){
        this.generaotr = generaotr;
        this.board=board;
        this.logic = logic;
    }

    public void initGame(){
        final Cell[][] cells = generaotr.generate();
        this.board.drawBoard(cells);
        this.logic.loadBoard(cells);
    }

    @Override
    public void select(int x, int y, boolean bomb) {
        this.logic.suggest(x,y,bomb);
        board.drawCell(x,y);
        if (this.logic.shouldBang(x,y)){
            this.board.drawBang();
        }
        if (this.logic.finish()){
            board.drawCongratulate();
        }


    }



}
