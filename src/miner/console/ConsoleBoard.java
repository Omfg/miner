package miner.console;

//import javafx.scene.control.miner.Cell;
//import javafx.scene.control.Boa

//import javafx.scene.control.miner.Cell;


import miner.Board;
import miner.Cell;

/**
 * Created by omfg on 13.11.16.
 */
public class ConsoleBoard implements Board {
    private Cell[][] cells;

    public void drawBoard(Cell[][] cells){

    this.cells =cells;
        this.redraw(false);

}
public void drawCell(int x, int y){
    System.out.println("******SELECT*******");
    this.redraw(true);

}
public void drawBang(){
    System.out.println("*****BANG*****");
    this.redraw(true);

}
public void drawCongratulate(){
    System.out.println("*****CONGARTULATIONS*****");
}
private void redraw(boolean real){
    for (Cell[] row:cells) {
        for (Cell cell:row
             ) { cell.draw(System.out,real);

        }
        System.out.println();

    }
    System.out.println();
}

}
