package miner;

/**
 * Created by omfg on 13.11.16.
 */
public interface Board {
    void drawBoard(Cell[][] cells);

    void drawCell(int c, int y);

    void drawBang();

    void drawCongratulate();
}
